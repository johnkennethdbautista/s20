// MAIN ACTIVITY
let number = Number(prompt("Please provide a number: "));
console.log("The number you provided is " + number);
for(; number >= 0; number--){
	if (number <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if(number % 5 === 0){
		console.log(number);
	}
	
}

// STRETCH GOAL
let stretchGoal = 'supercalifragilisticexpialidocious';
let consonants = '';
for(i = 0; i < stretchGoal.length; i++){
	if(stretchGoal[i].toLowerCase() == 'a' ||stretchGoal[i].toLowerCase() == 'e' ||stretchGoal[i].toLowerCase() == 'i' ||stretchGoal[i].toLowerCase() == 'o' ||stretchGoal[i].toLowerCase() == 'u'){
		continue;
	}else{
		consonants = consonants + stretchGoal[i];
	}
}
console.log(stretchGoal);
console.log(consonants);

